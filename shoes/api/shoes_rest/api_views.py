from django.http import JsonResponse
import json


# from wardrobe.api.wardrobe_api.models import Bin
from .models import Shoes, BinVO
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder



class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "import_href",
    ]


class ShoeListEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "manufacturer",
        "bin",
        "id",
        
    ]
    def get_extra_data(self, o):
        return {"bin": o.bin.id}

class ShoeDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "image_url",
        "bin",
        "id",
    ]
    # encoder = {
    #     "bin": BinVODetailEncoder()
    # }
    def get_extra_data(self, o):
        return {"bin": o.bin.id}




@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoes.objects.all()
        return JsonResponse (
            {"shoes": shoes},
            encoder = ShoeListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            bin_href = content["bin"]
            bin  = BinVO.objects.get(import_href = bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status = 400
            )
        shoes = Shoes.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder = ShoeDetailEncoder,
            safe = False,
        )
        
        
        
        

@require_http_methods(["GET", "PUT", "DELETE"])
def api_detail_shoes(request, pk):
    if request.method == "GET":
        shoes = Shoes.objects.get(id = pk)
        return JsonResponse(
            shoes,
            encoder=ShoeDetailEncoder,
            safe=False
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        Shoes.objects.filter(id=pk).update(**content)
        shoes = Shoes.objects.get(id=pk)
        return JsonResponse(
            shoes,
            encoder=ShoeDetailEncoder,
            safe = False
        )
    else:
        request.method == "DELETE"
        count, _ = Shoes.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
        