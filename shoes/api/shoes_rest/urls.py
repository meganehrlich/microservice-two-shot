from django.urls import path
from .api_views import api_list_shoes, api_detail_shoes


urlpatterns = [
    path("shoes/", api_list_shoes, name = "api_create_shoes" ),
    path("shoes/<int:pk>/", api_detail_shoes, name = "api_shoe_details")
]
