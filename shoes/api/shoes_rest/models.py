from django.db import models

class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=45)


class Shoes(models.Model):
    manufacturer = models.CharField(max_length=100)
    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=20)
    image_url = models.URLField(blank=True, null = True)
    bin = models.ForeignKey(
        BinVO,
        related_name="Shoes",
        on_delete=models.CASCADE,
    )
    
    def __str__(self):
        return self.model_name
