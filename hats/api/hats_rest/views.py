from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import Hats, LocationVO
from common.json import ModelEncoder



class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["import_href", "closet_name"]

class HatListEncoder(ModelEncoder):
    model = Hats
    properties = ["style_name", "location", "id"]

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}



class HatDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "style_name",
        "color",
        "fabric",
        "photo_url",
        "location",
        "id"
    ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def list_hats(request):
    if request.method == "GET":
        hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder
        )
    else:
        content = json.loads(request.body)

        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except:
            pass



        hat = Hats.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
        

            

@require_http_methods(["DELETE", "GET", "PUT"])
def show_hat(request, pk):
    if request.method == "GET":
        hat = Hats.objects.get(id=pk)
        
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        Hats.objects.filter(id=pk).update(**content)
        hat = Hats.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe = False
        )
    else:
        count, _ = Hats.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})