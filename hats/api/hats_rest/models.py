from django.db import models

# Create your models here.

class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)
    # section_number = models.SmallIntegerField
    # shelf_number = models.SmallIntegerField


class Hats(models.Model):

    style_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    fabric = models.CharField(max_length=200)
    photo_url = models.URLField(null=True, blank=True)
    # location = models.CharField(max_length=200)
    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.style_name
