# Wardrobify

Team:

* Person 1 - Which microservice?
* Person 2 - Megan Ehrlich Hats Microservice

* Person 1 Josh - Shoes
* Person 2 Megan - Hats

## Design

## Shoes microservice
install the shoes microservice in the settings
Create model that includes fields;
manufacturer
model_name
color
image url
the bin in the wardrope where it is dropdown menu 



create the view to access the model data
use react to;
 show list of shoes
 create a new shoe
 delete a shoe

React portion
keep track of routes in app.js and in Nav to make sure our links are working
Show shoes, all shoes and their details
1. manufacturer 
2. model_name
3. color
4. image_url
5. bin 
6. maybe closet

create, 
feilds are
1. manufacturer 
2. model_name
3. color
4. image_url
5. bin -- dropdown menu

on delete
delete button


## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

Django Steps:
Hat model should have “style_name”, “fabric”, “color”, “photo-url”, “location”
Will install app in project
Create view to list, add, and delete hat model
Configure URLS

React:
Build component to list hats in browser
Route the list component to appear when clicked on in nav bar
Add create functionality
Make sure I am able to loop over and add information to database
Add delete functionality including button for each item