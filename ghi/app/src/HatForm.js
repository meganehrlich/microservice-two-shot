import React from 'react';

class HatForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            style_name: '',
            color: '',
            fabric: '',
            photo_url: '',
            locations: [],
        };

        //this stuff
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeSName = this.handleChangeSName.bind(this);
        this.handleChangeColor = this.handleChangeColor.bind(this);
        this.handleChangeFabric = this.handleChangeFabric.bind(this);
        this.handleChangePhoto = this.handleChangePhoto.bind(this);
        this.handleChangeLocation = this.handleChangeLocation.bind(this);

    }
    async componentDidMount() {
        const url = 'http://localhost:8100/api/locations/';
        

        const response = await fetch(url);

        if (response.ok) {

            const data = await response.json();
            this.setState({ locations: data.locations})
            console.log(data.locations)
            


        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.locations;

        const hatsUrl = 'http://localhost:8090/api/hats/';
        const fetchOptions = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(hatsUrl, fetchOptions);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat)

            const cleared = {
                style_name: '',
                color: '',
                fabric: '',
                photo_url: '',
                location: '',
            };
            this.setState(cleared)
        }
    }
    handleChangeSName(event) {
        const value = event.target.value;
        this.setState({ style_name: value});
    }

    handleChangeColor (event) {
        const value = event.target.value;
        this.setState({ color: value });
    }

    handleChangeFabric (event) {
        const value = event.target.value;
        this.setState({ fabric: value });
    }

    handleChangePhoto (event) {
        const value = event.target.value;
        this.setState({ photo_url: value });
    }

    handleChangeLocation (event) {
        const value = event.target.value;
        this.setState({ location: value });
    }

    render() {

        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Log A New Hat</h1>
                <form onSubmit={this.handleSubmit} id="create-hat-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeSName} placeholder="Hat Style" value = {this.state.style_name} required type="text" name="style_name" id="style_name" className="form-control" />
                    <label htmlFor="name">Style Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeColor} placeholder="Color" value = {this.state.color} required type="text" name="color" id="color" className="form-control" />
                    <label htmlFor="starts">Color</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeFabric} placeholder="Fabric" value = {this.state.fabric} required type="text" name="fabric" id="fabric" className="form-control" />
                    <label htmlFor="ends">Fabric</label>
                  </div>
                  <div className="form-floating mb-3">
                    <label htmlFor="description">Photo URL</label>
                    <textarea onChange={this.handleChangePhoto} id="photo_url" value = {this.state.photo_url} required type="text" placeholder="Photo URL" name="photo_url" className="form-control" />
                  </div>
                  <div className="mb-3">
                    <select onChange={this.handleChangeLocation} value = {this.state.location} required name="location" id="location" className="form-select">
                      <option value="">Choose Location</option>
                      {this.state.locations.map(location => {
                        return (
                          <option key={location.id} value={location.href}>Section: {location.section_number} Shelf: {location.shelf_number} | {location.href}</option>
                          )
                          
                      })}
                    </select>
                  </div>
                  <button className="btn btn-primary">Create Hat</button>
                </form>
              </div>
            </div>
          </div>
        );
    }
}

export default HatForm
