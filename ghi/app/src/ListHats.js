import React from 'react';
import { Link } from 'react-router-dom';


function HatColumn(props) {
    // console.log(props)
    return (
        <div className="col">
            {props.list.map(data => {
                const hat = data;
                console.log(hat)
                return (
                <div key={hat.id} className="card mb-3 shadow">
                    <img src={hat.photo_url} className="card-img-top" />
                    <div className="card-body">
                        <h5 className="card-title">{hat.style_name}</h5>
                        <h6 className="card-subtitle">Color: {hat.color}    |    Fabric: {hat.fabric}</h6>
                        {/* <h6 className="card-subtitle">Fabric: {hat.fabric}</h6> */}
                        
                    </div>
                    <div className="card-footer">
                        <h6 className="card-subtitle mb-2">Location Information</h6>
                        <h6 className="card-subtitle mb-2 text-muted">Closet: {hat.location.closet_name}</h6>
                        <h6 className="card-subtitle mb-2 text-muted">Section Number: {hat.location.section_number}</h6>
                        <h6 className="card-subtitle mb-2 text-muted">Shelf Number: {hat.location.shelf_number}</h6>
                        <a onClick ={() => deleteHat(hat)} className="btn btn-primary btn-sm px-2 gap-3">Delete</a>
                    </div>
                </div>
                );
            })};
        </div>
    )
}


async function deleteHat(hats) {
    const hatUrl = `http://localhost:8090/api/hats/${hats.id}`;
    const fetchOptions = {
        method: 'delete',
        headers: {
        'Content-Type': 'application/json',
        },
    };console.log("Hat Removed")
    await fetch(hatUrl, fetchOptions);
    window.location.reload(true);
    
}


class ListHats extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hatColumns: [[], [], []]
        };
    }

    async componentDidMount() {

        const url = 'http://localhost:8090/api/hats/';
        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();

                const requests = [];
                for (let hat of data.hats) {
                    // console.log("Hello Sweetie")
                    // console.log(hat.id)
                    const detailUrl = `http://localhost:8090/api/hats/${hat.id}`;
                    // console.log(detailUrl)
                    requests.push(fetch(detailUrl));
                }
                const responses = await Promise.all(requests)

                const hatColumns = [[], [], []];

                let i = 0;
                for (const hatResponse of responses) {
                    if (hatResponse.ok) {
                        const details = await hatResponse.json();
                        hatColumns[i].push(details);
                        i = i + 1;
                        if (i > 2) {
                            i = 0
                        }
                    } else {
                        console.error(hatResponse);
                    }
                }
                this.setState({hatColumns: hatColumns});
            }
        } catch (e) {
            console.error(e);
        }

    }
    render() {
        return (
            <>
                
                <div className="px-4 py-5 my-5 mt-0 text-center bg-info">
                    <img className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="" width="600" />
                    <h1 className="display-5 fw-bold"> Hats Gallore </h1>
                    <div className="col-lg-6 mx-auto">
                        <p className="lead mb-4">
                        The only resource you'll ever need to organize your closet/s
                        </p>
                        <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                        <Link to="new" className="btn btn-primary btn-lg px-4 gap-3">Log A Hat</Link>
                    </div>
                    </div>
                </div>
                <div>
                    <h1>Fashionable Hats in Your Closet</h1>
                </div>
                <div className="container">
                    <div className="row">
                        {this.state.hatColumns.map((hatList, id) => {
                            return (
                                <HatColumn key={id} list={hatList} />
                            );
                        })}
                    </div>
                </div>
            </>
        );
    }
}



export default ListHats;


