import React from 'react';
import { Link } from 'react-router-dom';
import MainPage from './MainPage';
import { useState } from 'react';

function ShoesColumn(props){
    // console.log(props)
    return(

        <div className="col">
        {props.list.map(data => {
            const shoes = data;
            // console.log(data)
            return (
            <div key={shoes.id} className="card mb-3 shadow">
                <img src={shoes.image_url} className="card-img-top" />
                <div className="card-body">
                <h5 className="card-title">{shoes.manufacturer}</h5>
                <h6 className="card-subtitle mb-2 text-muted">
                    {shoes.model_name}
                </h6>
                <p className="card-text">
                    {shoes.color}
                </p>
                </div>
                
                <div className="card-footer">
                
                <p>Bin #{shoes.bin}</p>
                <a onClick ={() => deleteShoe(shoes)} className="btn btn-primary btn-sm px-2 gap-3">Delete</a>
                </div>
            </div>
            );
        })}
        
        </div>

    )
}
async function deleteShoe(shoes) {
    const shoeUrl = `http://localhost:8080/api/shoes/${shoes.id}`;
    const fetchOptions = {
        method: 'delete',
        headers: {
        'Content-Type': 'application/json',
        },
    };console.log("Shoe Deleted")
    await fetch(shoeUrl, fetchOptions);
    window.location.reload(true);
    
}







class ShoesList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        shoeColumns: [[], [], []],
    };
    }

    async componentDidMount() {
        const url = 'http://localhost:8080/api/shoes/';

        try {
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            const requests = [];
            for (let shoe of data.shoes) {
            const detailUrl = `http://localhost:8080/api/shoes/${shoe.id}`;
            requests.push(fetch(detailUrl));
            // console.log(detailUrl)
            }
            const responses = await Promise.all(requests);
            const shoeColumns = [[], [], []];
            let i = 0;
            for (const shoeResponse of responses) {
                if (shoeResponse.ok) {
                const details = await shoeResponse.json();
                shoeColumns[i].push(details);
                i = i + 1;
                if (i > 2) {
                i = 0;
                }
            } else {
                console.error(shoeResponse);
            }
            }

          // Set the state to the new list of three lists of
          // conferences
            this.setState({shoeColumns: shoeColumns});
        }
        } catch (e) {
        console.error(e);
        }
    }
  
    render() {
        return (
        <>
        <div className="px-4 py-5 my-5 mt-0 text-center bg-info">
            <img className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="" width="600" />
            <h1 className="display-5 fw-bold"> All Your Shoes in One Place</h1>
            <div className="col-lg-6 mx-auto">
                <p className="lead mb-4">
                The only resource you'll ever need to organize your closet/s
                </p>
                <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                <Link to="new-shoes" className="btn btn-primary btn-lg px-4 gap-3">Add some Shoes!</Link>
            </div>
            </div>
        </div>
        <div className="container">
            <h2>Your Shoes!</h2>
            <div className="row">
                {this.state.shoeColumns.map((shoeList, index) => {
                return (
                    <ShoesColumn key={index} list={shoeList} />
                );
                })}
            </div>
        </div>
        </>
    );
    }
}
export default ShoesList