import React from "react";


class CreateShoe extends React.Component {

//     const deleteShoe = () => {
//     fetch(`http://localhost:8080/api/shoes/${shoe.id}`, {
//         method: "DELETE",
//     }).then(() => {
//         console.log('Shoe deleted.')
//         setRefresh(true);
//     })
// }


    constructor(props) {
        // console.log(props)
        super(props)
        this.state = {
            manufacturer: '',
            model_name: '',
            color: '',
            image_url: '',
            bin: '',
            bins: [],
        }

        // this.state = {bin: [] }
        this.handleManufacturerChange = this.handleManufacturerChange.bind(this)
        this.handleBinChange = this.handleBinChange.bind(this)
        this.handleModelNameChange = this.handleModelNameChange.bind(this)
        this.handleColorChange = this.handleColorChange.bind(this)
        this.handleChangePhoto = this.handleChangePhoto.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleManufacturerChange(event){
        let value = event.target.value;
        this.setState({manufacturer: value})
    }

    handleBinChange(event){
        let value = event.target.value;
        this.setState({bin: value})
    }

    handleModelNameChange(event){
        let value = event.target.value;
        this.setState({model_name: value})
    }

    handleColorChange(event){
        let value = event.target.value;
        this.setState({color: value})
        console.log(event)
    }
    handleChangePhoto(event){
        let value = event.target.value;
        this.setState({image_url: value})
    }


    async handleSubmit(event) {

        event.preventDefault();
        const data = {...this.state };
        delete data.bins;


        let locationUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe)

            const cleared = {
                manufacturer: '',
                model_name: '',
                color: '',
                image_url: '',
                bin: '',

            };
            this.setState(cleared);
        }

    }
    async componentDidMount() {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            // console.log("data", data.bins)
            this.setState({ bins: data.bins });
            // console.log( "we want an array",typeof(data.bins))
            // console.log(data.bins)
        }
    }
    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new shoe</h1>
                    <form onSubmit={this.handleSubmit} id="create-shoe-form">
                        <div className="form-floating mb-3">
                            <input value = {this.state.manufacturer} onChange = {this.handleManufacturerChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                            <label htmlFor="manufacturer">Manufacturer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value = {this.state.model_name} onChange = {this.handleModelNameChange} placeholder="model_name" required type="text" name="model_name" id="model_name" className="form-control" />
                            <label htmlFor="model_name">Model Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value = {this.state.color} onChange = {this.handleColorChange} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                        <label htmlFor="description">Photo URL</label>
                        <textarea value = {this.state.image_url} onChange={this.handleChangePhoto} id="image_url" required type="text" placeholder="image_url" name="image_url" className="form-control" />
                        </div>
                        <div className="mb-3">
                            <select value = {this.state.bin} onChange = {this.handleBinChange} required id="bin" name="bin" className="form-select" >
                        <option>Choose a bin</option>
                        {this.state.bins.map(b => {
                        return (
                            <option key = {b.id} value = {b.href}>
                            {b.bin_number}
                            </option>
                        );
                        })}
                        </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
        )
    }

}
export default CreateShoe