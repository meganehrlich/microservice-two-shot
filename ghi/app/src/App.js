import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoesList';
import CreateShoe from './CreateShoe';

import ListHats from'./ListHats';
import HatForm from './HatForm';
// import HatDetails from './HatDetails';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path = "shoes/">
            <Route index element = {<ShoesList />} />
            <Route path = "new-shoes/" element = {<CreateShoe />} />
          </Route>
          <Route path="hats">
            <Route index element={<ListHats />} />
            <Route path="new" element={<HatForm />} />
            {/* <Route path="detail" element={<HatDetails />} /> */}
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
