Setup

- [x]  Fork repository from [https://gitlab.com/sjp19-public-resources/microservice-two-shot](https://gitlab.com/sjp19-public-resources/microservice-two-shot)
- [x]  Invite teammate Josh as maintaner
- [x]  Clone to computer
- [x]  Start virtual enviroment
- [x]  Create and move to branch 'hats'
- [x]  Run Docker commands:
    
    docker volume create pgdata
    docker-compose build
    docker-compose up
    
- [x]  Review current code with Josh
- [x]  Add names and microservice responsible for to README.md
- [x]  Add gameplan to README.md
- [x]  Start writing RESTful APIs

Django

- [x]  Install Django app into the Django project for your microservice `INSTALLED_APPS`
- [x]  Create Hat model
- [x]  Make one function view to show the list of your model.
- [x]  Configure the view in a URLs file that you will have to create for your Django app.
- [x]  Include the URLs from your Django app into your Django project's URLs.
- [x]  Test in Insomnia to see an empty list of your resources.
- [x]  Git your code in the **main** branch  
- [x]  Add to the function to handle POST requests to create a new record in your database.
- [x]  Use Insomnia to see if you can POST data to create a record.
- [x]  Use the GET to see if it's now in the list.
- [x]  Git your code in the **main** branch [Git In a Group](https://learn-2.galvanize.com/cohorts/3352/blocks/1885/content_files/build/03-project/66-practice-project.md).

React

- [x]  Build a React component to fetch the list and show the list of hats in the Web page.
    - [x]  Try to make it pretty, if you'd like.
- [x]  The React app has Bootstrap already in it.
- [x]  Route your list component to the appropriate path to show up when you click on the navigation bar link.
    - [x]  This is very much like either the `AttendeesList` component or the `MainPage` component from Conference GO!
    - [x]  make your own list component, like `ShoeList` or `HatList` and use that.
- [x]  Git your code in the **main** branch 
- [x]  keep adding little pieces of functionality.
- [x]  Keep going until you can successfully add, view, and delete your resource using the React UI using your microservice's back-end.
- [x]  Write your poller to get the `Bin` or `Location` data for your microservice.
- [x]  Use the **requests** library to make the HTTP request to the Wardrobe API
- [x]  loop over it and add it to your database.
    - [x]  This is like polled for `Account` information from attendees microservice in Conference GO!
- [x]  Leave the "delete" functionality for last.
- [x]  Then, add a delete button for each item in your list.
- [x]  If you give that button an `onClick` handler, you can use that to make a `fetch` with the HTTP method `DELETE`.
- [x]  You'll just need to figure out how to get the id of the thing you want to delete for the URL.
- [x]  To solve this, you could do something like this for the `onClick` handler, assuming you have a variable named `hat` that contains the `Hat` data.